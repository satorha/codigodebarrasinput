var forcarLeitorCodigoBarras = 
{
	tempo_entre_caracteres: 30,
	valor_anterior: "",
	tempo_anterior: null,
	input_widget: null,
	
	callbackLeitura: function () 
	{
		let caracteresAlterados = (this.input_widget.value.length - this.valor_anterior.length)					
		let tempo_atual = Date.now();		
		
		if(this.valor_anterior === "")
			this.tempo_anterior = tempo_atual;
		
		if((tempo_atual - this.tempo_anterior) > this.tempo_entre_caracteres || caracteresAlterados > 2)
		{
			this.input_widget.value = "";
			this.tempo_anterior = null;
			this.valor_anterior = "";
			return;
		}
		
		this.tempo_anterior = tempo_atual;
		this.valor_anterior = this.input_widget.value;
	},
	iniciarLeitura: function (inputid) 
	{
		this.input_widget = document.getElementById(inputid);
		this.input_widget.oninput = () => {this.callbackLeitura()};
	},			
};